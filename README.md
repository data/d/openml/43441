# OpenML dataset: The-Office-Dataset

https://www.openml.org/d/43441

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Office is an American Mockumentary sitcom television series that depicts the everyday lives of office employees in the Scranton, Pennsylvania, branch of the fictional Dunder Mifflin Paper Company.
Content
The dataset consists of 12 columns and 188 rows scrapped from IMDb. 
Acknowledgements
IMDb : https://www.imdb.com/title/tt0386676/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43441) of an [OpenML dataset](https://www.openml.org/d/43441). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43441/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43441/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43441/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

